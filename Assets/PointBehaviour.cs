﻿using UnityEngine;
using System.Collections;

public class PointBehaviour : MonoBehaviour {

	AudioSource hit;

	// Evento que dispara e acrescenta a pontuacao quando o jogador faz cesta
	void OnTriggerEnter(){
		Shoot.score += 1;
	}

	void OnCollisionEnter() {
		hit.Play ();

	}
}
