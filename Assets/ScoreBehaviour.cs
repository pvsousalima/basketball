﻿using UnityEngine;
using System.Collections;

public class ScoreBehaviour : MonoBehaviour {

	// Estilo do label
	public static GUIStyle guiStyle = new GUIStyle(); //create a new variable

	// Ao renderizar o jogo
	void OnGUI(){

		// Se o jogo nao tiver sido finalizado, renderiza os dados do jogo (pontos e arremessos disponiveis)
		if(!Shoot.finished){
		
			// Tamanho da fonte
			guiStyle.fontSize = 30;

			// Desenha um label relativo ao score no jogo
			GUI.Label (new Rect (100, 10, 100, 20), "Score:"+Shoot.score, guiStyle);

			// Desenha um label relativo a quantidade de arremesos no jogo
			GUI.Label (new Rect (900, 10, 100, 20), "Shoots:"+Shoot.availableShots, guiStyle);
		}
	}	 
		
}
