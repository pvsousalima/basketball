﻿using UnityEngine;
using System.Collections;

public class ForceBarBehaviour : MonoBehaviour {

	// propriedades da barra de forca
	public static float barDisplay; // progresso da barra
	public Vector2 pos = new Vector2(600,20); // posicao da barra de forca
	public Vector2 size = new Vector2(-20,600); // tamanho da barra de forca 
	public Texture2D emptyTex; // textura para barra de forca vazia
	public Texture2D fullTex; // textura para barra de forca cheia
	public float barTime; // tempo de preenchimento da barra de forca
	private GUIStyle guiStyle = new GUIStyle();  // Estilo do label de forca

	// Inicializacao
	void Start(){
		barTime = 0f;
	}

	// Ao desenhar o jogo
	void OnGUI () {

		// Se o jogo nao tiver sido finalizado
		if(!Shoot.finished){
		
			// Desenha a barra de forca (parte exterior)
			GUI.BeginGroup(new Rect(pos.x, pos.y, size.x, size.y));
			GUI.Box(new Rect(0,0, size.x, size.y), emptyTex);

			//desenha a barra de forca (parte interior)
			GUI.BeginGroup(new Rect(0,0, size.x * barDisplay, size.y));
			GUI.Box(new Rect(0,0, size.x, size.y), fullTex);
			GUI.EndGroup();
			GUI.EndGroup();

			// Tamanho da fonte
			guiStyle.fontSize = 30;

			// Desenha um label com a forca de arremesso
			GUI.Label (new Rect (500, 10, 100, 20), "Force:"+barDisplay * 100, guiStyle);

		}

	}
		

	// Funcao de atualizacao e chamada uma vez por frame
	void Update() {

		// atualiza o tempo de preenchimento da barra
		barTime += Time.deltaTime;

		//reseta o display
		if (barDisplay >= 1) { 
			barDisplay = 0; // percentual de preenchimento da barra
			barTime = 0f; // reseta o tempo de preenchimento da barra
		} else {
			barDisplay = barTime*0.4f; // preenche a barra de forca
		}
	}


}
