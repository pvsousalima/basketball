﻿using UnityEngine;
using System.Collections;

public class Shoot : MonoBehaviour {

	// Referencia para a bola do jogo
	public GameObject ball; 

	// Velocidade de tiro
	private Vector3 throwSpeed = new Vector3(0, 26, 40);

	// Posicao da bola
	public Vector3 ballPos; 

	// Flag para controle de arremessos
	private bool thrown = false; 

	// quantidade de arremessos disponiveis
	public static int availableShots = 5;

	// pontuacao do jogo
	public static int score = 0;

	//Jogo finalizado
	public GameObject Won;  // caso o jogador tenha vencido
	public GameObject gameOver;  // caso o jogador tenha perdido

	// se o jogo ja acabou
	public static bool finished = false;

	// Use this for initialization
	void Start () {
		
		// referencia a imagem de vencedor
		Won = GameObject.Find("won");

		// referencia a imagem de gameover
		gameOver = GameObject.Find ("gameOver");

		/* Increase Gravity */
		Physics.gravity = new Vector3(0, -600, 0);

		// Referencia ao objeto bola
		ball = GameObject.Find("ball");

		// congela a bola na mao do jogador
		ball.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezePositionY;
	}
	
	// Update is called once per frame
	void Update () {

		// Seta cenario de gameover como falso
		gameOver.SetActive(false);

		// Seta cenario de venceu como falso
		Won.SetActive(false);

		// Controle dos arremessos
		if (Input.GetKeyDown("space") && !thrown){

			// descongela a bola da mao do jogador para realizar o arremesso
			ball.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;

			// flag para controle de tiros
			thrown = true;

			// reduz em 1 a quantidade de arremessos
			availableShots--;

			// adiciona a forca e a velocidade de arremesso
			throwSpeed.y = throwSpeed.y + ForceBarBehaviour.barDisplay * 600;
			throwSpeed.z = throwSpeed.z + ForceBarBehaviour.barDisplay * 600;

			// adiciona a forca ao corpo redondo
			ball.GetComponent<Rigidbody>().AddForce(throwSpeed, ForceMode.Impulse);

			//audio.Play(); //play shoot sound
		}

		// caso o jogador perca
		if (availableShots == 0 && score < 3 && ball.transform.position.y < -500) {
			// jogador perdeu
			finished = true;
			gameOver.SetActive(true);
		} else {
			if(score>=3 && ball.transform.position.y < -500 && availableShots == 0){
				// jogador venceu
				Won.SetActive(true);
				finished = true;
			}
		}

		// Reinicia o arremesso quando a bola toca o chao
		if (ball != null && ball.transform.position.y < -500 && !finished){
			Invoke ("restart", 0);
		}

		// se o game foi finalizado
		if(finished){
			if (Input.GetKeyDown("space")){
				// reseta todos os atributos do jogo e reinicia o jogo
				Application.LoadLevel(0);
				finished = false;
				availableShots = 5;
				score = 0;
			}

		}
	
	}
		
	// reinicia a cena 
	void restart(){
		Application.LoadLevel(Application.loadedLevel);
	}

}
